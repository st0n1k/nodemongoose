const mongoose = require('mongoose');

const Dishes = require('./models/dishes');

const url = "mongodb+srv://admin:admin@cluster0-7sgao.mongodb.net/ConfusionDB?retryWrites=true&w=majority";
const connect = mongoose.connect(url);

connect.then(db => {
    console.log('Connected to server.');

    var newDish = Dishes({
        name: 'Uthappizza',
        description: 'test'
    })

    newDish.save()
        .then(dish => {
            console.log(dish);

            Dishes.find({}).exec();
        })
        .then(dishes => {
            console.log(dishes);

            return Dishes.remove({});
        })
        .then(() => {
            return mongoose.connection.close();
        })
        .catch(err => {
            console.log(err);
        })
});